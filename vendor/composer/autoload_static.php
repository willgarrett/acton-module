<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit7651a4e1094c6d5447e203b597426f46
{
    public static $prefixLengthsPsr4 = array (
        'w' => 
        array (
            'willgarrett\\' => 12,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'willgarrett\\' => 
        array (
            0 => __DIR__ . '/../..' . '/willgarrett',
        ),
    );

    public static $prefixesPsr0 = array (
        'U' => 
        array (
            'Unirest' => 
            array (
                0 => __DIR__ . '/..' . '/mashape/unirest-php/src',
            ),
        ),
    );

    public static $classMap = array (
        'Unirest\\Exception' => __DIR__ . '/..' . '/mashape/unirest-php/src/Unirest/Exception.php',
        'Unirest\\Method' => __DIR__ . '/..' . '/mashape/unirest-php/src/Unirest/Method.php',
        'Unirest\\Request' => __DIR__ . '/..' . '/mashape/unirest-php/src/Unirest/Request.php',
        'Unirest\\Request\\Body' => __DIR__ . '/..' . '/mashape/unirest-php/src/Unirest/Request/Body.php',
        'Unirest\\Response' => __DIR__ . '/..' . '/mashape/unirest-php/src/Unirest/Response.php',
        'willgarrett\\acton\\ActOn' => __DIR__ . '/../..' . '/willgarrett/acton/acton.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit7651a4e1094c6d5447e203b597426f46::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit7651a4e1094c6d5447e203b597426f46::$prefixDirsPsr4;
            $loader->prefixesPsr0 = ComposerStaticInit7651a4e1094c6d5447e203b597426f46::$prefixesPsr0;
            $loader->classMap = ComposerStaticInit7651a4e1094c6d5447e203b597426f46::$classMap;

        }, null, ClassLoader::class);
    }
}
