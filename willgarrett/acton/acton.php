<?php 
namespace willgarrett\acton;
use Unirest;

class ActOn{

    protected 
        $login_user, 
        $login_pass, 
        $client_id, 
        $client_secret, 
        $cur_token, 
        $cur_timeout, // time of token retrieval +3600 seconds
        $refresh_token,
        $domain;
    
    public function __construct($username = null, $password = null, $refresh = null)
    {
        $this->client_id = "CONFIGURATION VALUE"; // will@garrettassociates.net Act-On Developer
        $this->client_secret = "REPLACE WITH CONFIGURATION VALUE";

        $this->login_user = $username;
        $this->login_pass = $password;
        $this->domain = "https://restapi.actonsoftware.com/";
        if($refresh != null){
            // try auth with refresh token
            $this->refresh_token=$refresh;
            self::RefreshAuth();
        }
        else{
            // standard password authentication
            self::Authorize();
        }
    }
    public function LoadCredentials($token, $refresh_token){
        $this->cur_token=$token;
        $this->refresh_token=$refresh_token;
    }
    protected function Authorize()
    {
        // POST TOKEN REQUEST ENDPOINT
        $url = $this->domain.'token';

        // TOKEN REQUEST BODY
        $reqBody = array(
            'grant_type' => 'password',
            'username' => $this->login_user,
            'password' => $this->login_pass,
            'client_id' => $this->client_id,
            'client_secret' => $this->client_secret
        );

        // REQUEST HEADER
        $header = array(
            'Content-Type' => 'application/x-www-form-urlencoded'
        );
        // UNIREST BODY PACKAGING
        $data = Unirest\Request\Body::form($reqBody);
        
        // Generate Request
        $response = Unirest\Request::post($url, $header, $data);
        
        // Used for testing invalid responses
        //var_dump($response);
            // var_dump($response->body->error == invalid_grant)
                    // $response->code == 400
                // ONLY 5 LOGINS PER HOUR
        
        // Set Timeout based on response
        //echo $response->body->expires_in;
        $this->cur_timeout=(int)$response->body->expires_in+time();

        $this->cur_token = $response->body->access_token;
        $this->refresh_token = $response->body->refresh_token;
    }
    protected function refreshAuth()
    {
        $url = $this->domain.'token';
        $reqBody = array(
            'grant_type' => 'refresh_token',
            'refresh_token' => $this->refresh_token,
            'client_id' => $this->client_id,
            'client_secret' => $this->client_secret
        );
        $header = array(
            'Content-Type' => 'application/x-www-form-urlencoded'
        );
        // UNIREST BODY PACKAGING
        $data = Unirest\Request\Body::form($reqBody);
        
        // Generate Request
        $response = Unirest\Request::post($url, $header, $data);
        
        //var_dump($response);
        

        $this->cur_timeout=(int)$response->body->expires_in+time();
        $this->cur_token = $response->body->access_token;
        $this->refresh_token = $response->body->refresh_token;

    }
    protected function checkTimeOut(){
        if($this->cur_timeout >= time()){
            $this->refreshAuth();
        }
    }
    private function packageAuthHeader(){
        echo "Hit Auth Header";
        return array(
            'Authorization'=>'Bearer '.$this->cur_token,
            'Cache-Control'=>'no-cache'
            );
    }
    public function printCredentials() // used for testing
    { 
        echo "Username: ".$this->login_user."\n";
        echo "Password: ".$this->login_pass."\n";
        echo "Client ID: ".$this->client_id."\n";
        echo "Client Secret: ".$this->client_secret."\n";
        echo "Current Token: ".$this->cur_token."\n"; 
        echo "-- Cur Time: ".date('Y-m-d, H:i s e')."--".(int)time()."\n";
        echo "-- Timeout: ".date('Y-m-d, H:i s e', $this->cur_timeout)."--".(int)$this->cur_timeout."\n";
 //       echo "Token Timeout: ".$this->cur_timeout."\n"; // time of token retrieval +3600 seconds
        echo "Refresh Token: ".$this->refresh_token."\n";
    }
    public function getAccountInfo(){
        $url = $this->domain."api/1/account";
        //self::refreshAuth();
        $header=array('Authorization'=>'Bearer '.$this->cur_token,
            'Cache-Control'=>'no-cache');
        // $header = Unirest\Request\Header($this->packageAuthHeader());
        

        $response = Unirest\Request::get($url, $header, null);
        var_dump($response);
    }

} // end ActOn class

// finding the bug... about understanding why the bug was there to begin with. about knowing that its existence is not an accident... revelation